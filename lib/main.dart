import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:id3/id3.dart';
import 'package:html/parser.dart' show parse;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // inflates widget MyApp to the screen
  runApp(MyApp());
}

// Stateless widget = no dynamic content
class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _selectedIndex = 0;

  static List<Widget> _pages = <Widget>[
    //Icon(Icons.home, size: 150),
    AllSongsWidget(),
    AddMusicForm(),
    SearchMusicForm(),
    //Icon(Icons.add_circle_outline_sharp, size: 150)
  ];

  // called anytime the ui needs to be rebuilt
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text('Flutter Music App'),
      ),
      body: Center(
        child: _pages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home_filled), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.add_circle_outline_sharp), label: 'Add Songs'),
          BottomNavigationBarItem(
              icon: Icon(Icons.search), label: 'Search Songs'),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    ));
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}

class AllSongsWidget extends StatefulWidget {
  const AllSongsWidget({super.key});

  @override
  State<AllSongsWidget> createState() => _AllSongsWidgetState();
}

class _AllSongsWidgetState extends State<AllSongsWidget> {
  @override
  Widget build(BuildContext context) {
    // Code taken from / modified from learnflutterwithme.com/sqlite
    return Center(
        child: FutureBuilder<List<Song>>(
      future: DatabaseHelper.instance.getSongs(),
      builder: (BuildContext context, AsyncSnapshot<List<Song>> snapshot) {
        if (!snapshot.hasData) {
          return Center(child: Text('Loading...'));
        }
        return snapshot.data!.isEmpty
            ? Center(
                child: Text('No Songs Added.'),
              )
            : ListView(
                children: snapshot.data!.map((song) {
                return Center(
                  child: ListTile(
                    title: Text(song.title),
                    subtitle: Text(song.artist_name),
                  ),
                );
              }).toList());
      },
    ));
  }
}

class AddMusicForm extends StatefulWidget {
  const AddMusicForm({super.key});

  @override
  State<AddMusicForm> createState() => _AddMusicFormState();
}

class _AddMusicFormState extends State<AddMusicForm> {
  final controller = TextEditingController();
  int progress = 0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
                labelText: 'Music URL',
                hintText: 'Enter URL for song file or song directory'),
            controller: controller,
          ),
          ElevatedButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () async {
                // Should check if controller.text is a valid url
                final uriPath = Uri.parse(controller.text);
                // Not a single file
                if (uriPath.pathSegments.length == 0) {
                  // Look for files recursively
                  downloadRecursively(uriPath.toString());
                } else {
                  final filename =
                      uriPath.pathSegments[uriPath.pathSegments.length - 1];
                  downloadFile(controller.text, filename);
                }
              },
              child: const Text('Submit')),
          Text('$progress%'),
        ],
      ),
    );
  }

  Future<void> downloadFile(uri, filename) async {
    Dio dio = Dio();
    String file_location = await getFileLocation(filename);
    // Make sure this is downloading and writing before then runs
    try {
      dio
          .download(
        uri,
        file_location,
        onReceiveProgress: (count, total) => {
          setState(() => {progress = ((count / total) * 100).toInt()})
        },
      )
          .then((_) {
        addMetadataToDB(file_location);
      });
    } catch (e) {
      print("DOWNLOAD FAILED: $e");
    }
  }

  Future<void> addMetadataToDB(String file_location) async {
    // This runs after the download has been finished
    final metadata = getSongMetadata(file_location);
    if (checkValidMetadata(metadata)) {
      Song song = songFromMetadata(metadata, file_location);
      bool duplicate = await isDuplicate(song);
      if (!duplicate) {
        addSongToDB(song);
      } else {
        // We should only delete a duplicate file if the downloaded
        // file_location is different from the already existing db entry
      }
    } else {
      // If the metadata is invalid, then we can just delete the file.
      print("Deleting the file without valid metadata: $file_location");
      File(file_location).delete();
    }
  }

  Future<void> downloadRecursively(uri) async {
    // Get html page
    Dio dio = Dio();
    Response response;
    try {
      response = await dio.get(uri);
    } catch (e) {
      print("RECURSIVE GET FAILED: $e");
      return;
    }
    // Get all links
    var document = parse(response.data);
    var allLinks = document.getElementsByTagName("a");
    for (var i = 0; i < allLinks.length; i++) {
      // This is only tested to work on a host that is running python's web
      // server.  Directories are considered to end with /, and songs .mp3.
      // If the link doesn't end with either it is ignored.  Furthermore, they
      // are considered to be relative links.

      var linkDest = allLinks[i].attributes['href'];
      if (allLinks[i].attributes['href'] == null) {
        continue;
      }

      var link = allLinks[i].attributes['href']!.trim();
      if (link.endsWith(".mp3")) {
        // If links are links to song files, download them
        await downloadFile('$uri/$link', link);
        //print("FILE TO DOWNLOAD: $link");
      } else if (link.endsWith("/")) {
        // If links are links to directories, search them recursively (only on same
        // domain)
        await downloadRecursively('$uri/$link');
      }
    }
  }

  Future<String> getFileLocation(uniqueFileName) async {
    String path = '';

    Directory dir = await getApplicationDocumentsDirectory();
    path = '${dir.path}/$uniqueFileName';

    return path;
  }

  Future<void> printDownloadedFiles() async {
    Directory dir = await getApplicationDocumentsDirectory();
    List files = dir.listSync();
    for (var i = 0; i < files.length; i++) {
      print(files[i].uri);
    }
  }

  Future<void> printSongsFromDB() async {}

  Map<String, dynamic>? getSongMetadata(file_location) {
    List<int> mp3Bytes = File(file_location).readAsBytesSync();
    MP3Instance mp3instance = new MP3Instance(mp3Bytes);
    if (mp3instance.parseTagsSync()) {
      return mp3instance.getMetaTags();
    }
  }

  Song songFromMetadata(metadata, file_location) {
    Song song = Song(
        title: metadata['Title'],
        artist_name: metadata['Artist'],
        album_name: metadata['Album'],
        file_location: file_location,
        time_added: DateTime.now().toString());
    return song;
  }

  void addSongToDB(song) {
    DatabaseHelper.instance.addSong(song);
  }

  bool checkValidMetadata(Map<String, dynamic>? metadata) {
    if (metadata == null ||
        metadata.containsKey('Title') == false ||
        metadata.containsKey('Artist') == false ||
        metadata.containsKey('Album') == false) {
      return false;
    }
    return true;
  }

  // Check if the same song has already been entered into the DB
  Future<bool> isDuplicate(Song song) async {
    int matching = await DatabaseHelper.instance.getMatchingSong(song);
    return matching > 0;
  }
}

class SearchMusicForm extends StatefulWidget {
  const SearchMusicForm({super.key});

  @override
  State<SearchMusicForm> createState() => _SearchMusicFormState();
}

class _SearchMusicFormState extends State<SearchMusicForm> {
  final controller = TextEditingController();
  List<Song> songs = [];

  @override
  void initState() {
    super.initState();

    DatabaseHelper.instance.searchSong('').then((result) {
      setState(() {
        songs = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
                labelText: 'Search Song',
                hintText:
                    'Start searching for song titles, artists, or albums'),
            controller: controller,
          ),
          ElevatedButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () async {
                var newSongs =
                    await DatabaseHelper.instance.searchSong(controller.text);
                setState(() {
                  songs = newSongs;
                });
              },
              child: const Text('Submit')),
          Expanded(
              child: ListView.builder(
                  itemCount: songs.length,
                  itemBuilder: ((context, index) {
                    return ListTile(
                      title: Text(songs[index].title),
                      subtitle: Text(songs[index].artist_name),
                    );
                  }))),
        ],
      ),
    );
  }
}

// This basic DB code is taken from:
// learnflutterwithme.com/sqlite

// TODO:
// This should be using foreign keys to artist/album tables.  Also is
// missing the image field.
class Song {
  final int? id;
  final String title;
  final String artist_name;
  final String album_name;
  final String file_location;
  final String time_added;

  Song(
      {this.id,
      required this.title,
      required this.artist_name,
      required this.album_name,
      required this.file_location,
      required this.time_added});

  factory Song.fromMap(Map<String, dynamic> json) => new Song(
      title: json['title'],
      artist_name: json['artist_name'],
      album_name: json['album_name'],
      file_location: json['file_location'],
      time_added: json['time_added']);

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'artist_name': artist_name,
      'album_name': album_name,
      'file_location': file_location,
      'time_added': time_added
    };
  }
}

class DatabaseHelper {
  // Ensures that this class is a singleton -- that there's only one instance
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Static variables belong to the class rather than a specific instance.
  // _database will be the one instance of the database associated with
  // DatabaseHelper.  It is loaded lazily -- it can be null or an actual
  // database instance.
  static Database? _database;
  // Asynchronously get the database.  If null, we'll call the _initDatabase to
  // initialize the database and then return that instance.
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'musiclibrary.db');
    // Open the database.  If it doesn't exist, call _onCreate.
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      create table songs(
        id int primary key,
        title text not null,
        artist_name text not null,
        album_name text not null,
        file_location text not null,
        time_added datetime not null);
    ''');
  }

  Future<List<Song>> getSongs() async {
    Database db = await instance.database;
    var songs = await db.query('songs', orderBy: 'time_added DESC');
    List<Song> songList =
        songs.isNotEmpty ? songs.map((c) => Song.fromMap(c)).toList() : [];
    return songList;
  }

  Future<int> getMatchingSong(Song song) async {
    Database db = await instance.database;
    var songs = await db.query('songs',
        where: 'title = ? and artist_name = ? and album_name = ?',
        whereArgs: [song.title, song.artist_name, song.album_name]);
    return songs.length;
  }

  Future<List<Song>> searchSong(String name) async {
    Database db = await instance.database;
    name = "%$name%";
    var songs = await db.query('songs',
        where: "title LIKE ? OR artist_name LIKE ? OR album_name LIKE ?",
        whereArgs: [name, name, name]);
    List<Song> songList =
        songs.isNotEmpty ? songs.map((c) => Song.fromMap(c)).toList() : [];
    return songList;
  }

  Future<int> addSong(Song song) async {
    Database db = await instance.database;
    return await db.insert('songs', song.toMap());
  }

  Future<void> deleteAllSongs() async {
    Database db = await instance.database;
    await db.delete('songs');
  }
}
